# Non equilibrium Termodynamics physic simulation in python
## Description
This project is a deliverable from the subject Non equilibrium Termodynamics from the Fundamental Physics bachelor (Universidad Complutense de Madrid). The objective is to simulate the time evolution on a system witha given initial profile temperature, graphicate it and answer to the proposed theorical questions.
## Language
The project is entirely in spanish. Sadly, I do not have the original file and thus cannot translate it without too much hustle. Feel free to use any online translation tool such as DeepL. Hope you find it useful!
