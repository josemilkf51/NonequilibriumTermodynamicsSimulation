import numpy as np
#import math as m
import matplotlib.pyplot as pl

#%%
L=1
D=1
T1=1
T21=1.5
T22=2
T23=4
T24=10
N=65
a=L/N
h=1e-4# paso en s
tiempo=1#tiempo de la simulación en s
t=int(tiempo/h)
tline=np.linspace(0,tiempo,t-2)

def sigma(Tprima,Temp):
     s=Tprima**2/(Temp**2)
     return s
#%%
#------CASO-1------------------------------------------------------------------
#---------Sección-A------------------------------------------------------------
Temp=np.zeros([t,N]) #El tiempo va en las filas y el espacio en las columnas



Temp[0,:]=T1
Temp[:,0]=T1
Temp[:,-1]=T21


for i in range(0,t-1):
    for j in range(1,N-1):
        Temp[i+1,j]=Temp[i,j]+h*D*(Temp[i,j+1]+Temp[i,j-1]-2*Temp[i,j])/a**2
        

pl.imshow(Temp,extent=[0,L,tiempo,0],aspect='auto')
pl.ylabel("Tiempo (s)")
pl.xlabel("Posición (m)")
pl.title("Mapa de temperaturas de T2=1.5ºC")
pl.colorbar().ax.set_title("ºC")
pl.show()


#-----------------Sección-B----------------------------------------------------
#Cálculo producción entropía


Tx=np.zeros([t,N])
Smalla=np.zeros([t,N])

#Calculo dT/dx

for i in range(0,t):
    for j in range(1,N-1):
        Tx[i,j]=(Temp[i,j+1]-Temp[i,j-1])/(2*a)


for i in range(0,len(Smalla)):
    Smalla[i]=sigma(Tx[i],Temp[i])/4*Temp[i]

Smalla[:,-1]=Smalla[:,-2]#pequeña extrapolación para que quede mejor el dibujito
Smalla[:,0]=Smalla[:,1]#pequeña extrapolación para que quede mejor el dibujito
pl.imshow(Smalla,extent=[0,L,tiempo,0],vmax=30,aspect='auto')
pl.colorbar().ax.set_title("  $W/Km$")
pl.xlabel("Posición (m)")
pl.ylabel("Tiempo (s)")
pl.title("Producción de entropía de T2=1.5ºC")
pl.show()

pl.plot(Smalla[1,:],label="t=0s")
pl.plot(Smalla[int(t*0.001),:],label="t=0.001s")
pl.plot(Smalla[int(t*0.01),:],label="t=0.01s")
pl.plot(Smalla[int(t*0.1),:],label="t=0.1s")
pl.plot(Smalla[int(t)-1,:],label="t=1s")
pl.title("Producción de entropía de T2=1.5ºC")
pl.ylim([0,30])
pl.legend()
pl.show()

#--------Sección-C-------------------------------------------------------------

P=[]
for i in range(1,t-1):
    P.append(sum(Smalla[i,1:N-1])*a)

pl.plot(tline,P)
pl.ylim([0,0.4])
pl.xlabel("Tiempo (s)")
pl.ylabel("Producción total de entropía (W/K)")
pl.title("Producción total de entropía para T2=1.5ºC")
pl.show()
#%%
#------CASO-2------------------------------------------------------------------
#---------Sección-A------------------------------------------------------------
Temp=np.zeros([t,N]) #El tiempo va en las filas y el espacio en las columnas



Temp[0,:]=T1
Temp[:,0]=T1
Temp[:,-1]=T22


for i in range(0,t-1):
    for j in range(1,N-1):
        Temp[i+1,j]=Temp[i,j]+h*D*(Temp[i,j+1]+Temp[i,j-1]-2*Temp[i,j])/a**2
        

pl.imshow(Temp,extent=[0,L,tiempo,0],aspect='auto')
pl.ylabel("Tiempo (s)")
pl.xlabel("Posición (m)")
pl.title("Mapa de temperaturas de T2=2ºC")
pl.colorbar().ax.set_title("ºC")
pl.show()


#-----------------Sección-B----------------------------------------------------
#Cálculo producción entropía


Tx=np.zeros([t,N])
Smalla=np.zeros([t,N])

#Calculo dT/dx

for i in range(0,t):
    for j in range(1,N-1):
        Tx[i,j]=(Temp[i,j+1]-Temp[i,j-1])/(2*a)


for i in range(0,len(Smalla)):
    Smalla[i]=sigma(Tx[i],Temp[i])/4*Temp[i]

Smalla[:,-1]=Smalla[:,-2]#pequeña extrapolación para que quede mejor el dibujito
Smalla[:,0]=Smalla[:,1]#pequeña extrapolación para que quede mejor el dibujito
pl.imshow(Smalla,extent=[0,L,tiempo,0],vmax=30,aspect='auto')
pl.colorbar().ax.set_title("  $W/Km$")
pl.xlabel("Posición (m)")
pl.ylabel("Tiempo (s)")
pl.title("Producción de entropía de T2=2ºC")
pl.show()
pl.plot(Smalla[1,:],label="t=0s")
pl.plot(Smalla[int(t*0.001),:],label="t=0.001s")
pl.plot(Smalla[int(t*0.01),:],label="t=0.01s")
pl.plot(Smalla[int(t*0.1),:],label="t=0.1s")
pl.plot(Smalla[int(t)-1,:],label="t=1s")
pl.ylim([0,30])
pl.legend()
pl.title("Producción de entropía de T2=2ºC")
pl.show()
#--------Sección-C-------------------------------------------------------------

P=[]
for i in range(1,t-1):
    P.append(sum(Smalla[i,1:N-1])*a)

pl.plot(tline,P)
pl.ylim([0,1.5])
pl.xlabel("Tiempo (s)")
pl.ylabel("Producción total de entropía (W/K)")
pl.title("Producción total de entropía para T2=2ºC")
pl.show()

#%%
#------CASO-3------------------------------------------------------------------
#---------Sección-A------------------------------------------------------------
Temp=np.zeros([t,N]) #El tiempo va en las filas y el espacio en las columnas



Temp[0,:]=T1
Temp[:,0]=T1
Temp[:,-1]=T23


for i in range(0,t-1):
    for j in range(1,N-1):
        Temp[i+1,j]=Temp[i,j]+h*D*(Temp[i,j+1]+Temp[i,j-1]-2*Temp[i,j])/a**2
        

pl.imshow(Temp,extent=[0,L,tiempo,0],aspect='auto')
pl.ylabel("Tiempo (s)")
pl.xlabel("Posición (m)")
pl.title("Mapa de temperaturas de T2=4ºC")
pl.colorbar().ax.set_title("ºC")
pl.show()


#-----------------Sección-B----------------------------------------------------
#Cálculo producción entropía


Tx=np.zeros([t,N])
Smalla=np.zeros([t,N])

#Calculo dT/dx

for i in range(0,t):
    for j in range(1,N-1):
        Tx[i,j]=(Temp[i,j+1]-Temp[i,j-1])/(2*a)


for i in range(0,len(Smalla)):
    Smalla[i]=sigma(Tx[i],Temp[i])/4*Temp[i]

Smalla[:,-1]=Smalla[:,-2]#pequeña extrapolación para que quede mejor el dibujito
Smalla[:,0]=Smalla[:,1]#pequeña extrapolación para que quede mejor el dibujito
pl.imshow(Smalla,extent=[0,L,tiempo,0],vmax=30,aspect='auto')
pl.colorbar().ax.set_title("  $W/Km$")
pl.xlabel("Posición (m)")
pl.ylabel("Tiempo (s)")
pl.title("Producción de entropía de T2=4ºC")
pl.show()
pl.plot(Smalla[1,:],label="t=0s")
pl.plot(Smalla[int(t*0.001),:],label="t=0.001s")
pl.plot(Smalla[int(t*0.01),:],label="t=0.01s")
pl.plot(Smalla[int(t*0.1),:],label="t=0.1s")
pl.plot(Smalla[int(t)-1,:],label="t=1s")
pl.title("Producción de entropía de T2=4ºC")
pl.ylim([0,30])
pl.legend()
pl.show()
#--------Sección-C-------------------------------------------------------------

P=[]
for i in range(1,t-1):
    P.append(sum(Smalla[i,1:N-1])*a)

pl.plot(tline,P)
pl.ylim([0.5,2.5])
pl.xlabel("Tiempo (s)")
pl.ylabel("Producción total de entropía (W/K)")
pl.title("Producción total de entropía para T2=4ºC")
pl.show()
#%%
#------CASO-4------------------------------------------------------------------
#---------Sección-A------------------------------------------------------------
Temp=np.zeros([t,N]) #El tiempo va en las filas y el espacio en las columnas



Temp[0,:]=T1
Temp[:,0]=T1
Temp[:,-1]=T24


for i in range(0,t-1):
    for j in range(1,N-1):
        Temp[i+1,j]=Temp[i,j]+h*D*(Temp[i,j+1]+Temp[i,j-1]-2*Temp[i,j])/a**2
        

pl.imshow(Temp,extent=[0,L,tiempo,0],aspect='auto')
pl.ylabel("Tiempo (s)")
pl.xlabel("Posición (m)")
pl.title("Mapa de temperaturas de T2=10ºC")
pl.colorbar().ax.set_title("ºC")
pl.show()


#-----------------Sección-B----------------------------------------------------
#Cálculo producción entropía


Tx=np.zeros([t,N])
Smalla=np.zeros([t,N])

#Calculo dT/dx

for i in range(0,t):
    for j in range(1,N-1):
        Tx[i,j]=(Temp[i,j+1]-Temp[i,j-1])/(2*a)


for i in range(0,len(Smalla)):
    Smalla[i]=sigma(Tx[i],Temp[i])/4*Temp[i]

Smalla[:,-1]=Smalla[:,-2]#pequeña extrapolación para que quede mejor el dibujito
Smalla[:,0]=Smalla[:,1]#pequeña extrapolación para que quede mejor el dibujito
pl.imshow(Smalla,extent=[0,L,tiempo,0],vmax=30,aspect='auto')
pl.colorbar().ax.set_title("  $W/Km$")
pl.xlabel("Posición (m)")
pl.ylabel("Tiempo (s)")
pl.title("Producción de entropía de T2=10ºC")
pl.show()
pl.plot(Smalla[1,:],label="t=0s")
pl.plot(Smalla[int(t*0.001),:],label="t=0.001s")
pl.plot(Smalla[int(t*0.01),:],label="t=0.01s")
pl.plot(Smalla[int(t*0.1),:],label="t=0.1s")
pl.plot(Smalla[int(t)-1,:],label="t=1s")
pl.title("Producción de entropía de T2=10ºC")
pl.ylim([0,30])
pl.legend()
pl.show()
#--------Sección-C-------------------------------------------------------------
#%%
P=[]
for i in range(1,t-1):
    P.append(sum(Smalla[i,1:N-1])*a)

pl.plot(tline,P)
pl.ylim([3,10])
pl.xlabel("Tiempo (s)")
pl.ylabel("Producción total de entropía (W/K)")
pl.title("Producción total de entropía para T2=10ºC")
pl.show()





